<?php

//$test = 123;
//$res = $test ?? 'test is null';
//echo $res;
//
//$a = rand(0, 10);
//echo $a . '<br>';
//
//$a = 123;
//$b = 321;
//
//$array = []; // returns false
//
//if ($array) {
//    echo 'if';
//} else {
//    echo 'else';
//}
//
//echo '<br>';
//
//if ($a > $b) {
//    echo '1';
//} elseif ($a == $b) {
//    echo '2';
//} else {
//    echo '3';
//}
//
//$x = 7;
//while ($x < 10) {
//    echo $x . ' ';
//    $x++;
//}
//
//$x = 7;
//do {
//    echo $x . ' ';
//} while ($x++ < 10);
//
//echo '<br>' . $x;
//
//for ($x = 0; $x < 10; $x++) echo $x;
//echo '<br>' . $x;
//echo '<br>';
//for ($x = 0; $x++ < 10;) echo $x;
//
//echo '<br>';
//
//for ($x = 0; $x < 10; $x++):
//    echo $x;
//endfor;
//
//for ($x = 0; $x < 9; $x++) {
//    for ($y = 0; $y < 9; $y++) {
//        echo ($x + 1) * ($y + 1) . ' ';
//    }
//    echo '<br>';
//}
//
//echo '<br>';
//
//$table = '<table border="1">';
//for ($x = 0; $x < 10; $x++) {
//    $table .= '<tr>';
//    for ($y = 0; $y < 10; $y++) {
//        $table .= '<td>' . ($x + 1) * ($y + 1) . '</td>';
//    }
//    $table .= '</tr>';
//}
//$table .= '</table>';
//echo $table;

//-----Arrays

$web = ['html', 'css', 'js', 'php', 'mysql'];
//echo $web[0] . ' ';
//$web = array('html', 'css', 'js', 'php', 'mysql');
//echo $web[1] . ' ';
$count = count($web);
//
//$day = [];
//$day[] = '1';
//$day[] = '2';
//
//print_r($day);
//
//$about = [
//    'name' => 'Kate',
//    'age' => 19,
//];
//
//print_r($about);
//echo $about['name'] . '<br>';
//
//foreach ($about as $key => $value) {
//    echo $key . ' => ' . $value . '<br>';
//}
//
//foreach ($about as $value) {
//    echo $value . '<br>';
//}
//
//$about = ['Kate', 19];
//foreach ($about as $key => $value) {
//    echo $key . ' => ' . $value . '<br>';
//}
//
//$aboutLevelUp = [
//    ['level' => 'up',
//        'point' => 100,]
//];
//
//foreach ($aboutLevelUp as $key => $val) {
//    foreach ($val as $index => $item) {
//        echo $index . ' => ' . $item . '<br>';
//    }
//}


$array = ['Kate', 'Alex', 'Andrey'];

function myCount($array)
{
    $count = 0;
    echo 'foreach ';
    foreach ($array as $value) {
        $count++;
    }
//
//    echo 'while ';
//    while ($array[$count]) {
//        $count++;
//    }
//
//    echo 'for ';
//    for(; $array[$count]; $count) {
//        $count++;
//    }

    return $count;
}

//echo myCount($array);

//$array = [];
//
//for ($i = 0; $i < 100; $i++) {
//    if ($i % 2 == 1)
//        $array[] = $i;
//}
//
//$array = [];
//
//$i = 0;
//while ($i++ < 100) {
//    if ($i % 2 == 1)
//        $array[] = $i;
//}
//
//foreach ($array as $value) {
//    echo $value . ' ';
//}
//
//for ($i = 0; $i < 100; $i++) {
//    if($i == 50) {
//        echo ' --- it\'s break --- ';
//        break;
//    }
//
//    if($i == 20) {
//        echo ' --- go to $i++ --- ';
//        continue;
//    }
//    echo $i . ' ';
//}

$x = 1;

switch ($x) {
    case 1:
        echo '1';
        break;
    case 2:
        echo '2';
        break;
    case 3:
        echo '3';
        break;
    default:
        echo 'def';
        break;
}


?>

<!--<form action="">-->
<!--    <select name="" id="">-->
<!--        --><?php //while (--$count >= 0): ?>
<!--            <option value="--><? //= $web[$count] ?><!--">-->
<!--                --><? //= $web[$count] ?>
<!--            </option>-->
<!--        --><?php //endwhile; ?>
<!--    </select>-->
<!--</form>-->


<!---->
<!--<div>-->
<!--    --><?php //if (false): ?>
<!--        <div>-->
<!--            <p>-->
<!--                IF-->
<!--            </p>-->
<!--        </div>-->
<!--    --><?php //else: ?>
<!--        <div>-->
<!--            <p>-->
<!--                ELSE-->
<!--            </p>-->
<!--        </div>-->
<!--    --><?php //endif; ?>
<!--</div>-->
<!---->
<?php //$x = 0; ?>
<!---->
<!--<ul>-->
<!--    --><?php //while ($x++ < 10): ?>
<!--        <li>-->
<!--            --><? //= $x ?>
<!--        </li>-->
<!--    --><?php //endwhile; ?>
<!--</ul>-->
<!---->
<!---->
