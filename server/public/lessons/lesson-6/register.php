<form action="../Controller/ControllerRegister.php" method="post">
    <div class="container">
        <h1>Register to admin</h1>
        <div class="mb-3">
            <label for="first-name" class="form-label">First name</label>
            <input type="text" class="form-control" name="first-name" aria-describedby="emailHelp">
        </div>
        <div class="mb-3">
            <label for="last-name" class="form-label">Last name</label>
            <input type="text" class="form-control" name="last-name" aria-describedby="emailHelp">
        </div>
        <div class="mb-3">
            <label for="email" class="form-label">Email address</label>
            <input type="email" class="form-control" name="email" aria-describedby="emailHelp">
        </div>
        <div class="mb-3">
            <label for="password" class="form-label">Password</label>
            <input type="password" class="form-control" name="password">
        </div>
        <div class="mb-3 form-check">
            <input type="checkbox" class="form-check-input" name="checkbox">
            <label class="form-check-label" for="checkbox">Check me out</label>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</form>