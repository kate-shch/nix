<?php
//echo 'index start <br>';
//require - fatal error
//include - warning
//require_once 'login.php';
//require_once 'register.php';
//echo $page2 . '<br>'; // подчеркивает, но выводит
//echo 'index end <br>';

//function error($x)
//{
//    if (!$x) {
//        throw new Exception('Error is here!');
//    }
//}
//
//try {
//    error(0);
//    echo 'Everything is OK';
//} catch (Exception $exception) {
//    echo $exception->getMessage() . '<br>';
////    df($exception->getMessage());
//}
//$start = microtime(true);
//
//$date = date("Y-m-d G:i:s");
//$fp = fopen($_SERVER["DOCUMENT_ROOT"] . 'MyLog.txt', 'a+');
//
//for ($i = 0; $i < 10000; $i++) {
//    $str = $date . " " . print_r($i, true) . "\n";
//    fwrite($fp, $str);
//}
//fclose($fp);
//
//$time = microtime(true) - $start;
//echo $time;

//function declaration()
//{
//    return __FUNCTION__; // the name of the function
//}
//
//echo declaration();
//
//echo '<br>';
//
//$expression = function () {
//    echo 'expression';
//};
//
//$expression();
//
//echo '<br>';
//
//$arrow = fn() => 'arrow';
//echo $arrow();
//
//echo '<br>';
//
//function sumDecl($a, $b)
//{
//    return $a + $b;
//}
//
//echo sumDecl(1, 2);
//
//$sumExpression = function ($a, $b) {
//    echo $a + $b;
//};
//
//$sumExpression(1, 2);
//
//$arrow = fn($a, $b) => ($a + $b);
//echo $arrow(1, 2);
//
//function myMinDecl($a, $b, $c)
//{
//    $array[] = $a;
//    $array[] = $b;
//    $array[] = $c;
//    sort($array);
//    return $array[0];
//}
//
//echo myMinDecl(1, 3, 2);
//
//$myMinExp = function ($a, $b, $c) {
//    $array[] = $a;
//    $array[] = $b;
//    $array[] = $c;
//    sort($array);
//    return $array[0];
//};
//
//echo $myMinExp(1, 3, 2);

//$myMinArr = fn($a, $b, $c) => $array = [$a, $b, $c]; // not working

$array = ['Alex', 'Tolya', 'Kolya', 'Andrey'];

//$result = array_filter($array, function ($value) {
//    if ($value[0] == 'A') {
//        return $value;
//    }
//});
//
//print_r($result);
//
//function myFilter($array, $callback)
//{
//    $result = [];
//    foreach ($array as $value) {
//        if ($callback($value)) {
//            $result[] = $callback($value);
//        }
//    }
//    return $result;
//}
//
//$res = myFilter($array, function ($value) {
//    if ($value[0] == 'A') {
//        return $value;
//    }
//});
//
//print_r($res);
//
//$result = array_map(function ($value) {
//    return strtoupper($value);
//}, $array);

$result = array_map(fn($value) => strtoupper($value), $array);

print_r($result);

?>

<!--<!doctype html>-->
<!--<html lang="en">-->
<!--<head>-->
<!--    <meta charset="UTF-8">-->
<!--    <meta name="viewport"-->
<!--          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">-->
<!--    <meta http-equiv="X-UA-Compatible" content="ie=edge">-->
<!--    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"-->
<!--          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">-->
<!--    <title>Register to admin</title>-->
<!--</head>-->
<? //= include_once "login.php" ?>
<!--</html>-->


