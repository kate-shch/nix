<?php

// do /**
///**
// * @param string $str
// * @return string
// */
//function myTest(string $str): string // хороший тон
//{
////    for (; ;) {
////        if (true) {
////            return; // выйдет из функции
////        }
////    }
//    return $str;
//}
//
//echo myTest('Hi');
//

//!important
//$a = myTest();
//$a += 123;
//function myTest() ...
//not working

//function calcSquare(int $n = 10): int
//{
//    return $n ** 2;
//}
//
//$func = calcSquare();
//var_dump($func); // type - function

//function getName(string $name, string $surname = 'Shch'): bool
//{
//    echo "$name $surname ";
//    return true;
//}
//
//getName('Kate');

//function myEcho()
//{
//    echo 'number of params - ' . func_num_args() . '<br>';
//    $count = func_num_args();
//    for ($i = 0; $i < $count; $i++) {
//        echo func_get_arg($i) . '<br>';
//    }
//}

//function myEcho(...$array)
//{
//    print_r($array);
//}
//
//myEcho('First param', 'Second param', 'Third param', 123);

//$start = microtime();
//
//$a = 10;
//$b = &$a; //link
//$b = 0;
//echo "$a $b ";
//$a = 1;
//echo "$a $b<br>";
//
//$time = microtime(true) - $start;
//echo $time;

//$array = [];
//
//function testTime(array &$array)
//{
//    for ($i = 0; $i < 100000; $i++) {
//        $array[$i];
//    }
//}
//
//for ($i = 0; $i < 100000; $i++) {
//    $array[] = $i;
//}
//
//$start = microtime();
//testTime($array);
//$time = microtime(true) - $start;
//echo $time;
//
//$a = ['a' => 'aaa', 'b' => 'bbb'];
//$b = &$a['b'];
//$b = 123;
//echo $a['b'] . '<br>';
//$a['d'] = 890;
//print_r($a);

//$a = 123;
//$b = &$a;
//unset($a);
//
//var_dump($a);
//var_dump($b);

//function changeColor(string &$color) {
//    $color = 'blue';
//}
//$color = 'red';
//changeColor($color);
//echo $color;

//$num1 = 10;
//
//function &f1() {
//    global $num1;
//    return $num1;
//}
//
//$f2 = &f1();
//$f2--;
//echo $num1 . $f2 . f1();

//$num1 = 10;
//
//function f1()
//{
//    global $num1;
//    return $num1;
//}
//
//$f2 = f1();
//$f2--;
//echo $num1 . $f2 . f1();

//$one = 123;
//function test() {
//    global $one;
//    $one = 321;
//}
//test();
//echo $one;

// OR

//$one = 123;
//function test(&$one)
//{
//    $one = 321;
//}
//test($one);
//echo $one;
//
//echo "<pre>";
//print_r($GLOBALS);
//echo "</pre>";


//function myCount()
//{
//    static $count = 0;
//    $count++;
//    echo $count;
//}
//
//for ($i = 0; $i < 5; $i++) {
//    myCount();
//}
//myCount();
//myCount();
//myCount();

//function factor($n)
//{
//    static $count = 0;
//    $count++;
//    if ($n < 1) {
//        echo $count . '<br>';
//        return 1;
//    } else {
//        return $n * factor($n - 1);
//    }
//}
//
//echo factor(10);

//function dd($array)
//{
//    echo '<pre>';
//    print_r($array);
//    echo '</pre>';
//}
//
//$array = ['a', 'b', ['c']];
//
//dd($array);























