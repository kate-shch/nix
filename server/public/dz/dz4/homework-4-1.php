<?php

function br()
{
    echo '<br>';
}

function task(int $number)
{
    echo "<br>----- task $number -----<br>";
}

task(1);
function printPrimeNumbers(int $lastNumber)
{
    for ($i = 2; $i <= $lastNumber; $i++) {
        $f = true;
        for ($j = 2; $j < $i; $j++) {
            if ($i % $j == 0) {
                $f = false;
                break;
            }
        }

        if ($f) {
            echo $i . ' ';
        }
    }
}

printPrimeNumbers(20);

task(2);
$count = 0;
for ($i = 0; $i < 100; $i++) {
    $a = rand(0, 10);
    if ($a % 2 == 0) {
        $count++;
    }
}
echo $count;

task(3);
$counts = [];
for ($i = 1; $i <= 5; $i++) {
    $counts[$i] = 0;
}
$count = 0;
for ($i = 0; $i < 100; $i++) {
    $a = rand(1, 5);
    $counts[$a]++;
    if ($a == 5) $count++;
}
echo '1 - ' . $counts[1] . '<br>';
echo '2 - ' . $counts[2] . '<br>';
echo '3 - ' . $counts[3] . '<br>';
echo '4 - ' . $counts[4] . '<br>';
echo '5 - ' . $counts[5];

task(4);
$table = '<table border="1">';
for ($i = 0; $i < 3; $i++) {
    $table .= '<tr>';
    for ($j = 0; $j < 5; $j++) {
        switch ($i) {
            case 0:
                $table .= '<td bgcolor="#f0fff0">' . '&nbsp&nbsp&nbsp&nbsp' . '</td>';
                break;
            case 1:
                $table .= '<td bgcolor="#8a2be2">' . '&nbsp&nbsp&nbsp&nbsp' . '</td>';
                break;
            case 2:
                $table .= '<td bgcolor="#ffb6c1">' . '&nbsp&nbsp&nbsp&nbsp' . '</td>';
                break;
        }
    }
    $table .= '</tr>';
}
$table .= '</table>';
echo $table;

task(5);
$mounth = 9;
switch ($mounth) {
    case $mounth < 3 && $mounth > 0 || $mounth == 12:
        echo 'winter';
        break;
    case $mounth < 6 && $mounth > 2:
        echo 'spring';
        break;
    case $mounth < 9 && $mounth > 5:
        echo 'summer';
        break;
    case $mounth < 12 && $mounth > 8:
        echo 'autumn';
        break;
}

task(6);
$str = 'abcde';

if ($str[0] == 'a')
    echo 'yes';
else
    echo 'no';

task(7);
$str = '12345';

if ($str[0] == '1' || $str[0] == '2' || $str[0] == '3')
    echo 'yes';
else
    echo 'no';

task(8);
$test = true;
if ($test === true)
    echo 'yes';
else
    echo 'no';
br();
$test = false;
echo $test === true ? 'yes' : 'no';

task(9);
$arrayEn = ['mo', 'tu', 'we', 'th', 'fr', 'sa', 'su'];
$arrayRu = ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс'];

$lang = 'ru';
if ($lang == 'ru')
    print_r($arrayRu);
else
    print_r($arrayEn);
br();
$lang = 'en';
print_r($lang == 'ru' ? $arrayRu : $arrayEn);

task(10);
$clock = rand(0, 59);
echo 'clock - ' . $clock . '<br>';
if ($clock < 15) {
    echo 1;
} elseif ($clock < 30) {
    echo 2;
} elseif ($clock < 45) {
    echo 3;
} else {
    echo 4;
}
br();
echo $clock < 15 ? 1 : ($clock < 30 ? 2 : ($clock < 45 ? 3 : 4));

?>