<?php

/*
 * @author Kate Shcherbakova <katuxashcherbakova@gmail.com>
 */

function br()
{
    echo '<br>';
}

function task(int $number, $method)
{
    switch ($method) {
        case 'w':
            $method = 'while';
            break;
        case 'd':
            $method = 'do while';
            break;
        case 'f':
            $method = 'for';
            break;
        case 'fe':
            $method = 'foreach';
            break;
    }
    echo "<br>----- task $number - $method-----<br>";
}

function myCount(array $array, string $method = 'w'): int
{
    $count = 0;
    switch ($method) {
        case 'w':
            while ($array[$count]) {
                $count++;
            }
            break;
        case 'd':
            if (!$array[$count])
                break;
            do {
                $count++;
            } while ($array[$count]);
            break;
        case 'f':
            for (; $array[$count]; $count) {
                $count++;
            }
            break;
        case 'fe':
            foreach ($array as $value) {
                $count++;
            }
            break;
    }

    return $count;
}

function myReverse(array $array, string $method = 'w'): array
{
    $reversedArray = [];
    $length = myCount($array);
    switch ($method) {
        case 'w':
            while (--$length) {
                $reversedArray[] = $array[$length];
            }
            $reversedArray[] = $array[$length];
            break;
        case 'd':
            do {
                $length--;
                $reversedArray[] = $array[$length];
            } while ($array[$length - 1]);
            break;
        case 'f':
            for ($i = $length - 1; $i >= 0; $i--) {
                $reversedArray[] = $array[$i];
            }
            break;
        case 'fe':
            for ($i = 0; $i < $length; $i++) {
                $reversedArray[] = 0;
            }
            foreach ($array as $value) {
                $length--;
                $reversedArray[$length] = $value;
            }
            break;
    }

    return $reversedArray;
}

function convertStringToArray(string $str): array
{
    $count = 0;
    $array = [];
    while ($str[$count]) {
        $array[] = $str[$count];
        $count++;
    }
    return $array;
}

function convertArrayToString(array $array): string
{
    $str = '';
    foreach ($array as $item) {
        $str .= $item;
    }
    return $str;
}

function myToLowerCase(string $str, $method = 'w'): string
{
    $array = convertStringToArray($str);
    $count = myCount($array);
    $i = 0;
    $ordAlphabet = mb_ord('z') - mb_ord('Z');

    switch ($method) {
        case 'w':
            while ($array[$i]) {
                $ordItem = mb_ord($array[$i]);
                $newOrdLetter = $ordItem + $ordAlphabet;
                if ($newOrdLetter < 97 || $newOrdLetter > 122) {
                    $newOrdLetter = mb_ord($array[$i]);
                }
                $lowerCaseLetter = mb_chr($newOrdLetter);
                $item = $lowerCaseLetter;
                $array[$i] = $item;
                $i++;
            }
            break;
        case 'd':
            do {
                $ordItem = mb_ord($array[$i]);
                $newOrdLetter = $ordItem + $ordAlphabet;
                if ($newOrdLetter < 97 || $newOrdLetter > 122) {
                    $newOrdLetter = mb_ord($array[$i]);
                }
                $lowerCaseLetter = mb_chr($newOrdLetter);
                $item = $lowerCaseLetter;
                $array[$i] = $item;
                $i++;
            } while ($array[$i]);
            break;
        case 'f':
            for ($i = 0; $i < $count; $i++) {
                $ordItem = mb_ord($array[$i]);
                $newOrdLetter = $ordItem + $ordAlphabet;
                if ($newOrdLetter < 97 || $newOrdLetter > 122) {
                    $newOrdLetter = mb_ord($array[$i]);
                }
                $lowerCaseLetter = mb_chr($newOrdLetter);
                $item = $lowerCaseLetter;
                $array[$i] = $item;
            }
            break;
        case 'fe':
            foreach ($array as $index => $item) {
                $ordItem = mb_ord($item);
                $newOrdLetter = $ordItem + $ordAlphabet;
                if ($newOrdLetter < 97 || $newOrdLetter > 122) {
                    $newOrdLetter = mb_ord($item);
                }
                $lowerCaseLetter = mb_chr($newOrdLetter);
                $item = $lowerCaseLetter;
                $array[$index] = $item;
            }
            break;
    }

    return convertArrayToString($array);
}

function myToUpperCase(string $str, $method = 'w'): string
{
    $array = convertStringToArray($str);
    $count = myCount($array);
    $i = 0;
    $ordAlphabet = mb_ord('z') - mb_ord('Z');

    switch ($method) {
        case 'w':
            while ($array[$i]) {
                $ordItem = mb_ord($array[$i]);
                $newOrdLetter = $ordItem - $ordAlphabet;
                if ($newOrdLetter < 65 || $newOrdLetter > 90) {
                    $newOrdLetter = mb_ord($array[$i]);
                }
                $lowerCaseLetter = mb_chr($newOrdLetter);
                $item = $lowerCaseLetter;
                $array[$i] = $item;
                $i++;
            }
            break;
        case 'd':
            do {
                $ordItem = mb_ord($array[$i]);
                $newOrdLetter = $ordItem - $ordAlphabet;
                if ($newOrdLetter < 65 || $newOrdLetter > 90) {
                    $newOrdLetter = mb_ord($array[$i]);
                }
                $lowerCaseLetter = mb_chr($newOrdLetter);
                $item = $lowerCaseLetter;
                $array[$i] = $item;
                $i++;
            } while ($array[$i]);
            break;
        case 'f':
            for ($i = 0; $i < $count; $i++) {
                $ordItem = mb_ord($array[$i]);
                $newOrdLetter = $ordItem - $ordAlphabet;
                if ($newOrdLetter < 65 || $newOrdLetter > 90) {
                    $newOrdLetter = mb_ord($array[$i]);
                }
                $lowerCaseLetter = mb_chr($newOrdLetter);
                $item = $lowerCaseLetter;
                $array[$i] = $item;
            }
            break;
        case 'fe':
            foreach ($array as $index => $item) {
                $ordItem = mb_ord($item);
                $newOrdLetter = $ordItem - $ordAlphabet;
                if ($newOrdLetter < 65 || $newOrdLetter > 90) {
                    $newOrdLetter = mb_ord($item);
                }
                $lowerCaseLetter = mb_chr($newOrdLetter);
                $item = $lowerCaseLetter;
                $array[$index] = $item;
            }
            break;
    }

    return convertArrayToString($array);
}

function myArrayToLowerCase(array $array, $method = 'w'): array
{
    $count = myCount($array);
    $i = 0;

    switch ($method) {
        case 'w':
            while ($array[$i]) {
                $array[$i] = myToLowerCase($array[$i]);
                $i++;
            }
            break;
        case 'd':
            do {
                $array[$i] = myToLowerCase($array[$i]);
                $i++;
            } while ($array[$i]);
            break;
        case 'f':
            for ($i = 0; $i < $count; $i++) {
                $array[$i] = myToLowerCase($array[$i]);
            }
            break;
        case 'fe':
            foreach ($array as $index => $item) {
                $array[$index] = myToLowerCase($item);
            }
            break;
    }

    return $array;
}

function myArrayToUpperCase(array $array, $method = 'w'): array
{
    $count = myCount($array);
    $i = 0;

    switch ($method) {
        case 'w':
            while ($array[$i]) {
                $array[$i] = myToUpperCase($array[$i]);
                $i++;
            }
            break;
        case 'd':
            do {
                $array[$i] = myToUpperCase($array[$i]);
                $i++;
            } while ($array[$i]);
            break;
        case 'f':
            for ($i = 0; $i < $count; $i++) {
                $array[$i] = myToUpperCase($array[$i]);
            }
            break;
        case 'fe':
            foreach ($array as $index => $item) {
                $array[$index] = myToUpperCase($item);
            }
            break;
    }

    return $array;
}

function convertNumberToArray(int $number): array
{
    $array = [];

    while (intdiv($number, 10)) {
        $array[] = $number % 10;
        $number = intdiv($number, 10);
    }
    $array[] = $number % 10;

    return myReverse($array);
}

function convertArrayToNumber(array $array): int
{
    $array = myReverse($array);
    $number = 0;
    $zeros = 1;
    foreach ($array as $item) {
        $number += ($zeros * $item);
        $zeros *= 10;
    }

    return $number;
}

function bubbleSort(array $array): array
{
    for ($j = 0; $j < myCount($array) - 1; $j++) {
        for ($i = 0; $i < myCount($array) - $j - 1; $i++) {
            if ($array[$i] < $array[$i + 1]) {
                $tmp_var = $array[$i + 1];
                $array[$i + 1] = $array[$i];
                $array[$i] = $tmp_var;
            }
        }
    }

    return $array;
}

$array = [1, 2, 3, 4, 5];
task(1, 'w');
echo myCount($array, 'w');
task(1, 'd');
echo myCount($array, 'd');
task(1, 'f');
echo myCount($array, 'f');
task(1, 'fe');
echo myCount($array, 'fe');

br();

$array = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
task(2, 'w');
print_r(myReverse($array, 'w'));
task(2, 'd');
print_r(myReverse($array, 'd'));
task(2, 'f');
print_r(myReverse($array, 'f'));
task(2, 'fe');
print_r(myReverse($array, 'fe'));

br();

$array = [44, 12, 11, 7, 1, 99, 43, 5, 69];
task(3, 'w');
print_r(myReverse($array, 'w'));
task(3, 'd');
print_r(myReverse($array, 'd'));
task(3, 'f');
print_r(myReverse($array, 'f'));
task(3, 'fe');
print_r(myReverse($array, 'fe'));

br();

$str = 'Hi, I am ALex';
$array = convertStringToArray($str);
task(4, 'w');
$reverse = myReverse($array, 'w');
echo convertArrayToString($reverse);
task(4, 'd');
$reverse = myReverse($array, 'd');
echo convertArrayToString($reverse);
task(4, 'f');
$reverse = myReverse($array, 'f');
echo convertArrayToString($reverse);
task(4, 'fe');
$reverse = myReverse($array, 'fe');
echo convertArrayToString($reverse);

br();

$str = 'Hi, I am ALex';
task(5, 'w');
echo(myToLowerCase($str, 'w'));
task(5, 'd');
echo(myToLowerCase($str, 'd'));
task(5, 'f');
echo(myToLowerCase($str, 'f'));
task(5, 'fe');
echo(myToLowerCase($str, 'fe'));

br();

$str = 'Hi, I am ALex';
task(6, 'w');
echo(myToUpperCase($str, 'w'));
task(6, 'd');
echo(myToUpperCase($str, 'd'));
task(6, 'f');
echo(myToUpperCase($str, 'f'));
task(6, 'fe');
echo(myToUpperCase($str, 'fe'));

br();

$array = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
task(7, 'w');
print_r(myArrayToLowerCase($array, 'w'));
task(7, 'd');
print_r(myArrayToLowerCase($array, 'd'));
task(7, 'f');
print_r(myArrayToLowerCase($array, 'f'));
task(7, 'fe');
print_r(myArrayToLowerCase($array, 'fe'));

br();

$array = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
task(8, 'w');
print_r(myArrayToUpperCase($array, 'w'));
task(8, 'd');
print_r(myArrayToUpperCase($array, 'd'));
task(8, 'f');
print_r(myArrayToUpperCase($array, 'f'));
task(8, 'fe');
print_r(myArrayToUpperCase($array, 'fe'));

br();

$number = 12345678;
$array = convertNumberToArray($number);
task(9, 'w');
$reverse = myReverse($array, 'w');
echo convertArrayToNumber($reverse);
task(9, 'd');
$reverse = myReverse($array, 'd');
echo convertArrayToNumber($reverse);
task(9, 'f');
$reverse = myReverse($array, 'f');
echo convertArrayToNumber($reverse);
task(9, 'fe');
$reverse = myReverse($array, 'fe');
echo convertArrayToNumber($reverse);

br();

$array = [44, 12, 11, 7, 1, 99, 43, 5, 69];
task(9, 'f');
print_r(bubbleSort($array));

?>
