<?php
session_start();
require_once "homework-4-form.php";

if ($_SESSION['randNumber'] == null) {
    $_SESSION['randNumber'] = rand(5, 8);
}

$requestNumber = $_REQUEST['number'];

if ($requestNumber == '') {
    echo 'Please, don\'t leave the input form empty!';
} else {
    if ($requestNumber < 5) {
        echo '<p>Your number is too small!</p>';
    } elseif ($requestNumber > 8) {
        echo '<p>Your number is too big!</p>';
    } elseif ($requestNumber == $_SESSION['randNumber']) {
        $_SESSION['title'] = 'PLAY AGAIN';
        $_SESSION['balance']++;
        $_SESSION['randNumber'] = rand(5, 8);
        echo '<p>You guessed!</p>';
        echo '<p><a href="homework-4-homework-1.php">Play again<a></p>';
    } else {
        $_SESSION['title'] = 'TRY AGAIN';
        echo '<p><a href="homework-4-homework-1.php">Try again<a></p>';
    }
}


?>