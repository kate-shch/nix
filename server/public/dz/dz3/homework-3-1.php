<?php

session_start();

//task 1

if ($_REQUEST['name'] != '') {
    $_SESSION['name'] = $_REQUEST['name'];
}

//task 2

if ($_REQUEST['id'] != '' && $_REQUEST['amount'] != '') {
    $_SESSION['id'] = $_REQUEST['id'];
    $_SESSION['amount'] = $_REQUEST['amount'];
}

//task 3

if ($_REQUEST['client-name'] != '' && $_REQUEST['number'] != '') {
    $_SESSION['client-name'] = $_REQUEST['client-name'];
    $_SESSION['number'] = $_REQUEST['number'];
}

?>

<form action="<?= $_SERVER['SCRIPT_NAME'] ?>" method="post" enctype="multipart/form-data">
    <h3>Leave feedback</h3>
    <p>
        <label>
            Name
            <input type="text" name="name" value="<?= $_SESSION['name'] ?>">
        </label>
    </p>
    <p>
        <label>
            Comment
            <textarea name="comment"></textarea>
        </label>
    </p>
    <p>
        <input type="submit" name="submit" value="Go!">
    </p>
</form>

<br>

<form action="<?= $_SERVER['SCRIPT_NAME'] ?>" method="post" enctype="multipart/form-data">
    <h3>Add to cart</h3>
    <p>
        <label>
            Product ID
            <input type="text" name="id" value="<?= $_SESSION['id'] ?>">
        </label>
    </p>
    <p>
        <label>
            Amount
            <input type="text" name="amount" value="<?= $_SESSION['amount'] ?>">
        </label>
    </p>
    <p>
        <input type="submit" name="submit" value="Go!">
    </p>
</form>

<br>

<form action="<?= $_SERVER['SCRIPT_NAME'] ?>" method="post" enctype="multipart/form-data">
    <h3>Leave personal data</h3>
    <p>
        <label>
            Name
            <input type="text" name="client-name" value="<?= $_SESSION['client-name'] ?>">
        </label>
    </p>
    <p>
        <label>
            Phone number
            <input type="tel" name="number" value="<?= $_SESSION['number'] ?>">
        </label>
    </p>
    <p>
        <input type="submit" name="submit" value="Go!">
    </p>
</form>


<!--1. создать форму оставить отзыв-->
<!--1. поле имя-->
<!--2. поле комментарий-->
<!---->
<!--2. создать форму добавить товар в корзину-->
<!--1. id товара-->
<!--2. количество-->
<!---->
<!--3. обратная связь перезвонить клиенту-->
<!--1. имя-->
<!--2. телефон-->

