<?php

session_start();

if ($_REQUEST['name'] != '' && $_REQUEST['number'] != '' &&
    $_REQUEST['type'] != '' && $_REQUEST['address'] != '') {
    $_SESSION['name'] = $_REQUEST['name'];
    $_SESSION['number'] = $_REQUEST['number'];
    $_SESSION['type'] = $_REQUEST['type'];
    $_SESSION['address'] = $_REQUEST['address'];
}

?>

<form action="<?= $_SERVER['SCRIPT_NAME'] ?>" method="post" enctype="multipart/form-data">
    <h3>Place your order</h3>
    <p>
        <label>
            Name
            <input type="text" name="name" value="<?= $_SESSION['name'] ?>">
        </label>
    </p>
    <p>
        <label>
            Phone number
            <input type="text" name="number" value="<?= $_SESSION['number'] ?>">
        </label>
    </p>
    <p>
        <label>
            Type of delivery
            <input type="text" name="type" value="<?= $_SESSION['type'] ?>">
        </label>
    </p>
    <p>
        <label>
            Address
            <input type="text" name="address" value="<?= $_SESSION['address'] ?>">
        </label>
    </p>
    <p>
        <input type="submit" name="submit" value="Go!">
    </p>
</form>

<!--1. создать форму оформить заказ-->
<!--1. имя-->
<!--2. телефон-->
<!--3. тип доставки-->
<!--4. адрес доставки-->
<!--5. сессия получить список id товаров и передать в форму-->

