<?php

$valid_types = array('pdf', 'docx', 'xls', 'png', 'jpg');
if (isset($_FILES['userfile'])) {
    if (is_uploaded_file($_FILES['userfile']['tmp_name'])) {
        $filename = basename($_FILES['userfile']['name']);
        $ext = substr($_FILES['userfile']['name'], 1 + strrpos($_FILES['userfile']['name'], '.'));
        if (!in_array($ext, $valid_types)) {
            echo 'Invalid file type';
        } else {
            $uploaddir = $_SERVER['DOCUMENT_ROOT'] . '/img/' . $filename;
            move_uploaded_file($_FILES['userfile']['tmp_name'], $uploaddir);
            $src = '/img/' . $filename;
            echo "<img src='$src' alt='$filename' title='$filename'/>";
        }
    } else {
        echo 'Empty file';
    }
} else {
    echo '
	<form enctype="multipart/form-data" method="post">
	<p><input name="userfile" type="file">
	<input type="submit" value="Send file"></p>
	</form>';
}
?>

<!--1. Загружать файлы на сервер, попробуйте разрешить загрузку файлов только определенного формата-->
<!--1. pdf-->
<!--2. excel-->
<!--3. word-->
<!---->
<!--для изображений реализуй-->
<!---->
<!--1. png-->
<!--2. jpg-->