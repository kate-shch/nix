<!--1. все переменные выводим через echo/print/print_r-->
<!--2. создать переменную строку ваше имя и вывести на экран-->
<!--3. создать переменную возраст и вывести на экран-->
<!--4. создать переменную и присвоить ей число pi вывести переменную на экран-->
<!--5. создать переменную массив ['alex', 'vova', 'tolya'] вести ее на экран-->
<!--6. создать переменную массив ['alex', 'vova', 'tolya', ['kostya', 'olya']] вести ее на экран-->
<!--7. создать переменную массив ['alex', 'vova', 'tolya', ['kostya', 'olya', ['gosha', mila]]] вести ее на экран-->
<!--8. создать переменную массив [['alex', 'vova', 'tolya'], ['kostya', 'olya'], ['gosha', mila]] вести ее на экран-->

<?php
(string)$name = 'Kate';
echo "$name <br>";
(int)$age = 19;
echo "$age <br>";
$pi = pi();
echo "$pi";

$arr1 = ['alex', 'vova', 'tolya'];
echo '<pre>' , var_dump($arr1) , '</pre>';

$arr2 = ['alex', 'vova', 'tolya', ['kostya', 'olya']];
echo '<pre>' , var_dump($arr2) , '</pre>';

$arr3 = ['alex', 'vova', 'tolya', ['kostya', 'olya', ['gosha', 'mila']]];
echo '<pre>' , var_dump($arr3) , '</pre>';

$arr4 = [['alex', 'vova', 'tolya'], ['kostya', 'olya'], ['gosha', 'mila']];
echo '<pre>' , var_dump($arr4) , '</pre>';

?>



