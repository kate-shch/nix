<?php
/*
 * @author Kate Shcherbakova <katuxashcherbakova@gmail.com>
 */
echo '<pre>';

echo '*<br><br>';

$a = 42;
$b = 55;

echo '1. ', $a > $b ? "a($a) > b($b)<br>" : "a($a) < b($b)<br>";

$a = rand(1, 10);
$b = rand(1, 10);

echo '2. ', $a > $b ? "a($a) > b($b)<br>" : "a($a) < b($b)<br>";

$f = 'Shcherbakova';
$i = 'Kate';
$o = 'Andreevna';

//echo "3. $f $i[0]. $o[0]. <br>";

$r = rand(1, 999999);
$substr = substr_count($r, '5');

echo "4. $r includes $substr fives<br>";

$a = 3;
echo "5.<br> a = $a<br>";

$a = 10;
$b = 2;
echo ' a + b = ' . ($a + $b) . '   a - b = ' . ($a - $b) . '   a * b = ' . ($a * $b) .
    '   a / b = ' . ($a / $b) . '<br>';

$c = 15;
$d = 2;
$result = $c + $d;
echo " result = $result<br>";

$a = 10;
$b = 2;
$c = 5;
echo ' a + b + c = ' . ($a + $b + $c) . '<br>';

$a = 17;
$b = 10;
$c = $a - $b;
$d = $c;
echo " c = $c   d = $d<br>";

$result = $c + $d;
echo "6. result = $result <br>";

$text = 'Hello, world!';
echo "7. <br> $text <br>";

$text1 = 'Hello, ';
$text2 = 'world!';
echo ' ' . $text1 . $text2 . '<br>';

$seconds = 60;
$minutes = 60;
$hours = 24;
$days = 30;

echo ' Seconds in 30 days : ' . ($seconds * $minutes * $hours * $days) . '<br>';

$var = 1;
$var += 12;
$var -= 14;
$var *= 5;
$var /= 7;
$var %= 1;
echo '8. ' . $var . '<br>';

echo '<br>**<br><br>';

$seconds = date('s');
$minutes = date('i');
$hours = date('h');

echo "1. $hours:$minutes:$hours<br>";

$text = 'Я';
$text .= ' хочу';
$text .= ' знать';
$text .= ' PHP!';
echo '2. ' . $text . '<br>';

$foo = 'bar';
$bar = 10;

echo '3. ' . $$foo . '<br>';

$a = 2;
$b = 4;
echo '4. <br>';
echo ' ' . ($a++ + $b) . '<br>';
$a = 2;
$b = 4;
echo ' ' . ($a + ++$b) . '<br>';
$a = 2;
$b = 4;
echo ' ' . (++$a + $b++) . '<br>';

$a = 'variable';
echo '5. <br>type of variable : ' . gettype($a) . '<br>';
echo isset($a) ? 'exists' . '<br>' : 'not exists' . '<br>';
echo is_null($a) ? 'null' . '<br>' : 'not null' . '<br>';
echo empty($a) ? 'empty' . '<br>' : 'not empty' . '<br>';
echo is_integer($a) ? 'integer' . '<br>' : 'not integer' . '<br>';
echo is_double($a) ? 'double' . '<br>' : 'not double' . '<br>';
echo is_string($a) ? 'string' . '<br>' : 'not string' . '<br>';
echo is_numeric($a) ? 'numeric' . '<br>' : 'not numeric' . '<br>';
echo is_bool($a) ? 'boolean' . '<br>' : 'not boolean' . '<br>';
echo is_scalar($a) ? 'scalar' . '<br>' : 'not scalar' . '<br>';
echo is_array($a) ? 'array' . '<br>' : 'not array' . '<br>';
echo is_object($a) ? 'object' . '<br>' : 'not object' . '<br>';

echo '<br>***<br><br>';

$a = rand(1, 10);
$b = rand(1, 10);

echo '1. ' . 'a = ' . $a . '   b = ' . $b . '   sum = ' . ($a + $b) .
    '   product = ' . ($a * $b) . '<br>';

echo '2. sum of squares = ' . ($a * $a + $b * $b) . '<br>';
echo '3. average = ' . (($a + $b) / 2) . '<br>';

$x = 1;
$y = 2;
$z = 3;
echo '4. (x+1)−2(z−2x+y) = ' . (($x + 1) - 2 * ($z - 2 * $x + $y)) . '<br>';

$a = 10;
echo '5.<br> a % 3 = ' . ($a % 3) . '    a % 5 = ' . ($a % 5) . '<br>';
echo ' a + a * 0.3 = ' . ($a + $a * 0.3) .
    '    a + a * 1.2 = ' . ($a + $a * 1.2) . '<br>';

$a = 10;
$b = 20;
echo '6. ' . ($a * 0.4 + $b * 0.84) . '<br>';

$a = '123';
$a[1] = 0;
$b = $a[2] . $a[1] . $a[0];
echo '7. ' . $b . '<br>';

$a = 1;

echo '8. ' . ($a % 2 == 0 ? 'even' : 'odd') . '<br>';

echo '</pre>';


